module Zombutt

using Profile
using VT100
using VT100.FixedPointNumbers
using VT100: RGB8
using DataStructures

include("render.jl")

"""
    Ambiance

A struct detailing the interesting properties of a board location.
"""
struct Ambiance
  scent::UInt8
end
Ambiance() = Ambiance(0)


"""
    Piece

A piece on a board. The field `thing` points to the actual object of interest.
"""
struct Piece
  kind::Symbol
  ambiance::Ambiance
  thing
end
Piece() = Piece(nothing)
Piece(x) = Piece(get_kind(x), Ambiance(), x)

const Board = Matrix{Piece}

get_kind(piece::Piece) = get_kind(piece.kind)
get_cell(piece::Piece) = get_cell(piece.thing)

"""
    get_padded_view(board, i, j)

Get a view of `board` which only shows the region within the field-of-view centered on `(i,j)`.
"""
function get_padded_view(board, i, j)
  # FIXME: _board = 
  board
end

function move_piece!(board, pos, move)
  move == (0,0) && return
  i,j = pos
  piece = board[i,j]
  new_pos = pos .+ move
  if board[new_pos[1],new_pos[2]].thing === nothing
    set_piece!(board, pos, Piece())
    set_piece!(board, new_pos, piece)
  else
    collide!(board, pos, piece.thing, new_pos, board[new_pos[1],new_pos[2]].thing)
  end
end

update!(piece::Piece, board, pos) = update!(piece.thing, board, pos)
update!(thing, board, pos) = (0,0)
function set_piece!(board, pos, piece)
  i,j = pos
  _piece = Piece(piece.kind, board[i,j].ambiance, piece.thing)
  board[i,j] = _piece
end
function set_ambiance!(board, (i,j), ambiance)
  piece = board[i,j]
  board[i,j] = Piece(piece.kind, ambiance, piece.thing)
end

get_kind(::Nothing) = :environment
get_cell(::Nothing) = Cell(' ')

"""
    Wall

A very robust wall. Such strong, much boring, wow.
"""
struct Wall end

get_kind(wall::Wall) = :environment
get_cell(wall::Wall) = Cell(Cell('#'); fg=:green, flags=VT100.Flags.FG_IS_256)

"""
    Barrier

A constructable barrier. Zombies can (and will) break this down if you're on the other side of one of these.
"""
mutable struct Barrier
  strength::Float64
end
Barrier() = Barrier(0.1)

get_kind(barrier::Barrier) = :environment
function get_cell(barrier::Barrier)
  color = RGB8(N0f8(1.0), N0f8(1.0), N0f8(barrier.strength))
  Cell(Cell('#'); fg_rgb=color, flags=VT100.Flags.FG_IS_RGB)
end

"""
    Player

The lonely player. Let's see how long this one lasts...
"""
struct Player
  # FIXME: Complicated AI stuff
  #data
end

global const PLAYER_MOVE = Ref{Union{Tuple{Int,Int},Nothing}}(nothing)
global const PLAYER_ACTION = Ref{Union{Tuple{Int,Int},Nothing}}(nothing)

get_kind(player::Player) = :player
get_cell(player::Player) = Cell(Cell('@'); fg=:blue, flags=VT100.Flags.FG_IS_256)

function update!(player::Player, board, pos)
  if PLAYER_MOVE[] !== nothing
    move = PLAYER_MOVE[]
    PLAYER_MOVE[] = nothing
    return move
  elseif PLAYER_ACTION[] !== nothing
    action = PLAYER_ACTION[]
    PLAYER_ACTION[] = nothing
    new_pos = pos .+ action
    if board[new_pos[1], new_pos[2]].thing === nothing
      set_piece!(board, new_pos, Piece(Barrier()))
    elseif board[new_pos[1], new_pos[2]].thing isa Barrier
      strength = board[new_pos[1], new_pos[2]].thing.strength
      strength = clamp(strength+0.1, 0.0, 1.0)
      board[new_pos[1], new_pos[2]].thing.strength = strength
    end
  end
  return (0, 0)
end

"""
    Zombie

A common zombie. Likes brains. Usually stupid (but not always!).
"""
struct Zombie
  path::Vector{Tuple{Int,Int}}
end
Zombie() = Zombie(Tuple{Int,Int}[])

get_kind(zombie::Zombie) = :zombie
get_cell(zombie::Zombie) = Cell(Cell('X'); fg=:red, flags=VT100.Flags.FG_IS_256)

struct AstarNode #{AN}
  pos::Tuple{Int,Int}
  steps::Int
  prev #::AN
end
dist(pos1, pos2) = round(Int, sqrt((pos1[1]-pos2[1])^2 + (pos1[2]-pos2[2])^2))
function plan_astar(board, src, dst)
  open_set = PriorityQueue{AstarNode,Int}()
  closed_set = Set{Tuple{Int,Int}}()
  cost = dist(src,dst)
  enqueue!(open_set, AstarNode(src, 0, nothing), cost)
  while true
    # Dequeue lowest priority node
    node = dequeue!(open_set)

    # Add neighbors to open_set
    for oj in -1:1:1
      for oi in -1:1:1
        oi == oj == 0 && continue
        ni = node.pos[1]+oi
        nj = node.pos[2]+oj
        (ni < 1 || ni > size(board,1)) && continue
        (nj < 1 || nj > size(board,2)) && continue
        (ni,nj) in closed_set && continue
        board[ni,nj].thing isa Wall && continue
        steps = 1+node.steps
        cost = steps+dist((ni,nj),dst)
        enqueue!(open_set, AstarNode((ni,nj), steps, node), cost)
        #print("\e[$(nj);$(ni)H\e[0mO")
        #@info "Enqueued $((ni,nj)) => $cost || ($(length(open_set)) - $(length(closed_set)))"
      end
    end

    # If goal, return node
    if all(node.pos .== dst)
      return node
    else
      push!(closed_set, node.pos)
      #print("\e[$(node.pos[2]);$(node.pos[1])H\e[0mX")
    end
    #sleep(0.1)
  end
  error("Could not find path from $src to $dst")
end
function generate_moves(node, pos)
  path = Tuple{Int,Int}[]
  current = node
  while true
    current.prev === nothing && break
    push!(path, current.pos .- current.prev.pos)
    current = current.prev
  end
  return path
end
function update!(zombie::Zombie, board, pos)
  if isempty(zombie.path)
    # FIXME: Ascend along scent gradient instead
    dst = findfirst(piece->piece.thing isa Player, board).I
    if dist(pos, dst) > 20
      return (rand(-1:1:1),rand(-1:1:1))
    end
    goal_node = plan_astar(board, pos, dst)
    path = generate_moves(goal_node, pos)
    append!(zombie.path, path)
  end
  move = clamp.(pop!(zombie.path) .+ (rand(-1:1:1),rand(-1:1:1)), Ref(-1), Ref(1))
  if move[1] != 0 && move[2] != 0
    move = rand() > 0.5 ? (move[1], 0) : (0, move[2])
  end
  return move
end

"""
    Mine

A landmine. Watch where you step!
"""
struct Mine end

get_kind(mine::Mine) = :environment
get_cell(mine::Mine) = Cell(Cell('▲'); fg=:red, flags=VT100.Flags.FG_IS_256)

### Collision Logic ###

# Nothing changes when you hit a wall
collide!(board, pos, thing, new_pos, wall::Wall) = ()

# Break down the wall!
function collide!(board, pos, thing, new_pos, barrier::Barrier)
  barrier.strength -= 0.2
  if barrier.strength <= 0.0
    set_piece!(board, new_pos, Piece())
  end
end

# Zombies don't affect each other
collide!(board, pos, zombie1::Zombie, new_pos, zombie2::Zombie) = ()

# Destroy anything that steps on a mine, including the mine
function collide!(board, pos, thing, new_pos, mine::Mine)
  if thing isa Player
    error("Game Over!")
  else
    set_piece!(board, pos, Piece())
    set_piece!(board, new_pos, Piece())
  end
  # FIXME: Destroy whatever steps on this
end

# End game
function collide!(board, pos, zombie::Zombie, new_pos, player::Player)
  # FIXME: End game more cleanly?
  error("Game Over!")
end

### Initialization and Runtime Logic ###

"""
    new_board()

Initializes a new Board.
"""
function new_board(sz::Tuple{Int,Int})
  board = fill(Piece(:environment, Ambiance(), nothing), sz[1], sz[2])

  # Spawn the player
  pos = div.(size(board), Ref(2))
  board[pos[1], pos[2]] = Piece(Player())

  # FIXME: Create walls more intelligently
  for j in axes(board, 2)
    for i in axes(board, 1)
      if i == 1 || j == 1 || i == size(board, 1) || j == size(board, 2)
        # Border wall along perimeter
        board[i,j] = Piece(Wall())
        continue
      end
      if rand() < 0.05 && board[i,j].thing === nothing
        board[i,j] = Piece(Wall())
      end
    end
  end

  # Create some zombies
  for z in 1:10
    while true
      pos = (rand(1:size(board, 1)), rand(1:size(board, 2)))
      if board[pos[1],pos[2]].thing === nothing
        board[pos[1], pos[2]] = Piece(Zombie())
        break
      end
    end
  end

  # Create some mines
  for m in 1:rand(30:100)
    while true
      pos = (rand(1:size(board, 1)), rand(1:size(board, 2)))
      if board[pos[1],pos[2]].thing === nothing
        board[pos[1], pos[2]] = Piece(Mine())
        break
      end
    end
  end
  board
end

"""
    update!(board::Board)

Updates the specified board for a single timestep.
"""
function update!(board::Board)
  _board = copy(board)
  for j in axes(board, 2)
    for i in axes(board, 1)
      piece = board[i,j]

      # Update piece position
      move = update!(piece, get_padded_view(_board, i, j), (i,j))
      move_piece!(_board, (i,j), move)

      # Update ambiance
      new_scent = piece.ambiance.scent
      for oj in -1:1:1
        for oi in -1:1:1
          ni = i+oi
          nj = j+oj
          (ni < 1 || ni > size(board,1)) && continue
          (nj < 1 || nj > size(board,2)) && continue
          # FIXME: Do the (proper) thing to new_scent
          if board[ni,nj].kind == :player
            new_scent = 100
          else
            new_scent = clamp(new_scent - 1, 0, 255)
          end
        end
      end
      set_ambiance!(_board, (i,j), Ambiance(new_scent))
    end
  end
  board .= _board
end

"""
    render(io::IO, board::Board)

Draws a colored ASCII representation of `board` onto `io`.
"""
function render(io::IO, board::Board)
  iob = IOBuffer()
  for j in axes(board, 2)
    for i in axes(board, 1)
      cell = get_cell(board[i,j].thing)
      # FIXME: Set ambiance color
      color = RGB8(N0f8(0), N0f8(0), N0f8(board[i,j].ambiance.scent, 0))
      cell = Cell(cell; bg_rgb=color, flags=cell.flags | VT100.Flags.BG_IS_RGB)
      render_cell(iob, cell)
    end
    println(iob)
  end
  print(io, String(iob.data))
end

"""
    profile()

Profile run of the Zombutt package.
"""
function profile()
  board = new_board(displaysize(stdout)[2:-1:1] .- 2)
  try
    Profile.clear()
    update!(board)
    for i in 1:500
      Profile.@profile update!(board)
      #render(stdout, board)
    end
  finally
    Profile.print()
  end
end

"""
    demo()

Demos the Zombutt package.
"""
function demo()
  board = new_board(displaysize(stdout)[2:-1:1] .- 2)
  running = true
  @async begin
    try
      while running
        char = Char(read(stdin, 1)[1])
        if char == 'w'
          PLAYER_MOVE[] = (0, -1)
        elseif char == 's'
          PLAYER_MOVE[] = (0, 1)
        elseif char == 'a'
          PLAYER_MOVE[] = (-1, 0)
        elseif char == 'd'
          PLAYER_MOVE[] = (1, 0)
        elseif char == 'i'
          PLAYER_ACTION[] = (0, -1)
        elseif char == 'k'
          PLAYER_ACTION[] = (0, 1)
        elseif char == 'j'
          PLAYER_ACTION[] = (-1, 0)
        elseif char == 'l'
          PLAYER_ACTION[] = (1, 0)
        else
          running = false
          break
        end
      end
    finally
      running = false
    end
  end
  print(stdout, "\e[2J")
  print(stdout, "\e[?25l")
  run(`stty isig -icanon -echo min 1`)
  try
    while running
      print(stdout, "\e[1;1H")
      update!(board)
      render(stdout, board)
      sleep(0.1)
    end
  finally
    running = false
    run(`stty cooked echo`)
    print(stdout, "\e[?25h")
  end
end

end # module

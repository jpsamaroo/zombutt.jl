function render_cell(io::IO, cell::VT100.Cell)
  # FIXME: More attrs
  if cell.attrs & VT100.Attributes.Blink > 0
    print(io, "\e[5m")
  end
  if cell.flags & VT100.Flags.FG_IS_RGB > 0
    r, g, b = Int(floor(cell.fg_rgb.r*255)), Int(floor(cell.fg_rgb.g*255)), Int(floor(cell.fg_rgb.b*255))
    print(io, "\e[38;2;$(r);$(g);$(b)m")
  elseif cell.flags & VT100.Flags.FG_IS_256 > 0
    if 0 <= cell.fg <= 7
      print(io, "\e[3$(string(cell.fg))m")
    elseif 8 <= cell.fg <= 15
      print(io, "\e[9$(string(cell.fg-8))m")
    else
      print(io, "\e[38;5;$(string(cell.fg))m")
    end
  else
    print(io, "\e[39m")
  end

  if cell.flags & VT100.Flags.BG_IS_RGB > 0
    r, g, b = Int(floor(cell.bg_rgb.r*255)), Int(floor(cell.bg_rgb.g*255)), Int(floor(cell.bg_rgb.b*255))
    print(io, "\e[48;2;$(r);$(g);$(b)m")
  elseif cell.flags & VT100.Flags.BG_IS_256 > 0
    if 0 <= cell.bg <= 7
      print(io, "\e[4$(string(cell.bg))m")
    elseif 8 <= cell.bg <= 15
      print(io, "\e[10$(string(cell.bg-8))m")
    else
      print(io, "\e[48;5;$(string(cell.bg))m")
    end
  else
    print(io, "\e[49m")
  end

  print(io, cell.content)
end
